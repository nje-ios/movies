//
//  DetailsViewController.swift
//  Movies
//
//  Created by Test User on 2019. 11. 08..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var movieId: Int?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let movieId = movieId {
            print("Selected movie id: \(movieId)")
            DispatchQueue.global().async {
                self.getMovie(movieId: movieId)
            }
        }
    }
    
    func getMovie(movieId: Int) {
        let urlSession = URLSession(configuration: .default)
        let url = URL(string: "\(TheMovieDbConstants.ApiBaseUrl)movie/\(movieId)?api_key=\(TheMovieDbConstants.ApiKey)")
        
        let dataTask = urlSession.dataTask(with: url!, completionHandler: getMovieCompletionHandler(data:response:error:))
        dataTask.resume()
    }
    
    func getMovieCompletionHandler(data: Data?, response: URLResponse?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
            showErrorMessage()
        } else if
            let data = data,
            let response = response as? HTTPURLResponse {
            
                if response.statusCode == 200 {
                    if let movie = self.parseGetMovieJSONResponse(data: data) {
                        displayMovieDetails(movie: movie)
                    } else {
                        showErrorMessage()
                    }
                } else {
                    print("Response status code: \(response.statusCode)")
                    showErrorMessage()
                }
            }
    }
    
    private func parseGetMovieJSONResponse(data: Data) -> MovieDetailsResponse? {
        let jsonDecoder = JSONDecoder()
        let response = try? jsonDecoder.decode(MovieDetailsResponse.self, from: data)
        
        if let response = response {
            return response
        }
        
        return nil
    }
    
    func displayMovieDetails(movie: MovieDetailsResponse) {
        DispatchQueue.main.async {
            self.titleLabel.text = movie.title
        }
        
        guard let posterPath = movie.posterPath else {
            print("PosterPath is nil")
            return
        }
        
        guard let imageUrl = URL(string: TheMovieDbConstants.ImageBaseUrl + posterPath) else {
            print("Image URL is nil")
            return
        }
        
        guard let imageData = try? Data(contentsOf: imageUrl) else {
            print("There is no image data")
            return
        }

        DispatchQueue.main.async {
            self.imageView.image = UIImage(data: imageData)
        }
    }
}
