//
//  TheMovieDbConstants.swift
//  Movies
//
//  Created by Test User on 2019. 11. 10..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import Foundation

class TheMovieDbConstants {
    static let ApiKey = "<insert your themoviedb.org api key here>"
    static let ApiBaseUrl = "https://api.themoviedb.org/3/"
    static let ImageBaseUrl = "https://image.tmdb.org/t/p/original"
}
