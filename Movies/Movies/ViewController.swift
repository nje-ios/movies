//
//  ViewController.swift
//  Movies
//
//  Created by Test User on 2019. 09. 13..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var movies: [PopularMovie] = []
    
    @IBOutlet weak var movieTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        movieTableView.isHidden = true
        showSpinner(onView: self.view)
        DispatchQueue.global().async {
            self.getPopularMovies()
        }
    }

    func getPopularMovies() {
        let urlSession = URLSession(configuration: .default)
        
        let url = URL(string: "\(TheMovieDbConstants.ApiBaseUrl)movie/popular?api_key=\(TheMovieDbConstants.ApiKey)")
        
        let dataTask = urlSession.dataTask(with: url!, completionHandler: getPopularMoviesCompletionHandler(data:response:error:))
        
        dataTask.resume()
    }
    
    func getPopularMoviesCompletionHandler(data: Data?, response: URLResponse?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
            handleError()
        } else if
            let data = data,
            let response = response as? HTTPURLResponse {
            
            if response.statusCode == 200 {
                if let movies = self.parseGetPopularMoviesJSONResponse(data: data) {
                    self.movies = movies
                    displayPopularMovies()
                } else {
                    handleError()
                }
            } else {
                print("Response status code: \(response.statusCode)")
                handleError()
            }
        }
    }
    
    private func parseGetPopularMoviesJSONResponse(data: Data) -> [PopularMovie]? {
        let jsonDecoder = JSONDecoder()
        let response = try? jsonDecoder.decode(PopularMoviesResponse.self, from: data)
        
        if let response = response {
            return response.results
        }
        
        return nil
    }
    
    func displayPopularMovies() {
        DispatchQueue.main.async {
            self.removeSpinner()
            self.movieTableView.isHidden = false
            self.movieTableView.reloadData()
        }
    }
    
    func handleError() {
        DispatchQueue.main.async {
            self.removeSpinner()
            self.movieTableView.isHidden = false
            self.showErrorMessage()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let movie = movies[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath)
        cell.imageView?.image = "📽".image()
        cell.accessoryType = .disclosureIndicator
        cell.detailTextLabel?.text = movie.releaseDate
        cell.textLabel?.text = movie.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row) selected")
        performSegue(withIdentifier: "ListToDetails", sender: movies[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsViewController = segue.destination as! DetailsViewController
        let movie = sender as! PopularMovie
        detailsViewController.movieId = movie.id
    }
}

extension String {
    func image() -> UIImage? {
        let size = CGSize(width: 40, height: 40)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.white.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 40)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

// http://brainwashinc.com/2017/07/21/loading-activity-indicator-ios-swift/
var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    func showErrorMessage() {
        showAlert(title: "Error", message: "Oops, something went wrong.")
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        
        self.present(alertController, animated: true, completion: nil)
    }
}
